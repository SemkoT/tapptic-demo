package com.xstmpx.numberslight.rest.model

data class NumbersLight(
    val name: String,
    val image: String
)