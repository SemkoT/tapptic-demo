package com.xstmpx.numberslight.rest

import com.xstmpx.numberslight.rest.model.NumbersLight
import com.xstmpx.numberslight.rest.model.NumbersLightDetail
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface NumbersLightService {

    @GET("json.php")
    fun getNumbersLightList(): Single<List<NumbersLight>>

    @GET("json.php")
    fun getNumberLightDetail(@Query("name") name: String): Single<NumbersLightDetail>
}