package com.xstmpx.numberslight.rest.model

data class NumbersLightDetail(
    val name: String,
    val text: String,
    val image: String
)