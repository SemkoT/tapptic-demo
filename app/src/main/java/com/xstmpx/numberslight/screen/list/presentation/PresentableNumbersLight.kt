package com.xstmpx.numberslight.screen.list.presentation

import com.xstmpx.numberslight.rest.model.NumbersLight

data class PresentableNumbersLight(
    val name: String,
    val image: String
) {

    companion object {
        private const val IMAGE_NORMAL_SIZE = "&size=60"

        fun from(numbersLight: NumbersLight): PresentableNumbersLight {
            val imageNormal = numbersLight.image + IMAGE_NORMAL_SIZE
            return PresentableNumbersLight(numbersLight.name, imageNormal)
        }
    }
}