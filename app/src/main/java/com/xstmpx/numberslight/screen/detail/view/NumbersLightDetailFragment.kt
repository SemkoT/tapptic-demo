package com.xstmpx.numberslight.screen.detail.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import com.xstmpx.numberslight.R
import com.xstmpx.numberslight.screen.common.view.ProgressFragment
import com.xstmpx.numberslight.screen.detail.presentation.NumbersLightDetailPresenter
import com.xstmpx.numberslight.utils.alert.MessageAlert
import com.xstmpx.numberslight.utils.alert.MessageAlertListener
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_detail.*
import javax.inject.Inject

class NumbersLightDetailFragment : ProgressFragment(), NumbersLightDetailView, MessageAlertListener {

    companion object {
        private const val EXTRA_NAME = "EXTRA_NAME"

        fun createExtras(name: String): Bundle {
            val bundle = Bundle()
            bundle.putString(EXTRA_NAME, name)
            return bundle
        }
    }

    @Inject
    lateinit var presenter: NumbersLightDetailPresenter

    private var name: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidSupportInjection.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_detail, container, false)
        name = arguments?.getString(EXTRA_NAME)
        presenter.bind(this)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.onViewCreated(name ?: return)
    }

    override fun onDestroy() {
        presenter.unbind()
        super.onDestroy()
    }

    override fun showErrorDialog() {
        MessageAlert.with(requireContext(), getString(R.string.error), getString(R.string.request_error), this).show()
    }

    override fun showImage(image: String) {
        Picasso.get()
            .load(image)
            .fit()
            .centerInside()
            .placeholder(R.drawable.image_placeholder)
            .error(R.drawable.image_error)
            .into(imageView)
    }

    override fun showName(name: String) {
        textViewName.text = name
    }

    override fun showText(text: String) {
        textViewText.text = text
    }

    override fun onRetryClicked() {
        presenter.onRetryClicked()
    }
}