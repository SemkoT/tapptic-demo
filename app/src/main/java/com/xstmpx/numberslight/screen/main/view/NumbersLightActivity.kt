package com.xstmpx.numberslight.screen.main.view

import android.content.res.Configuration
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import com.xstmpx.numberslight.R
import com.xstmpx.numberslight.screen.detail.view.NumbersLightDetailFragment
import com.xstmpx.numberslight.screen.list.view.NumbersLightListFragment
import com.xstmpx.numberslight.screen.main.presentation.NumbersLightPresenter
import com.xstmpx.numberslight.utils.canGoBackOnFragmentBackStack
import com.xstmpx.numberslight.utils.isTabletInLandscape
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.actionbar.*
import javax.inject.Inject

class NumbersLightActivity : AppCompatActivity(), NumbersLightView {
    @Inject
    lateinit var presenter: NumbersLightPresenter

    companion object {
        private const val TAG_LIST_FRAGMENT = "TAG_LIST_FRAGMENT"
        private const val TAG_DETAIL_FRAGMENT = "TAG_DETAIL_FRAGMENT"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidInjection.inject(this)
        setContentView(R.layout.activity_numberslight)
        initActionBar()
        presenter.bind(this)
        presenter.onCreate()
        initFragments(savedInstanceState)
        initBackStackChangeListener()
        goBackIfIsTabletInLandscape()
    }

    private fun goBackIfIsTabletInLandscape() {
        if (isTabletInLandscape() && canGoBackOnFragmentBackStack()) {
            onBackPressed()
        }
    }

    private fun initBackStackChangeListener() {
        supportFragmentManager.addOnBackStackChangedListener {
            showOrHideBackButton()
        }
    }

    private fun showOrHideBackButton() {
        supportActionBar?.setDisplayHomeAsUpEnabled(canGoBackOnFragmentBackStack())
    }

    private fun initFragments(savedInstanceState: Bundle?) {
        if (savedInstanceState == null) {
            val listFragment = NumbersLightListFragment()
            listFragment.arguments = intent.extras
            replaceFragment(listFragment, R.id.fragmentContainerPrimary, TAG_LIST_FRAGMENT)
        }
    }

    private fun initActionBar() {
        setSupportActionBar(actionbar)
        showOrHideBackButton()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                presenter.onHomeClicked()
                return true
            }
        }
        return false
    }

    fun selectName(name: String) {
        presenter.selectName(name)
    }

    override fun showDetailScreen(name: String) {
        val detailFragment = NumbersLightDetailFragment()
        detailFragment.arguments = NumbersLightDetailFragment.createExtras(name)
        addOrReplaceDetails(detailFragment)
    }

    private fun addOrReplaceDetails(detailFragment: NumbersLightDetailFragment) {
        if (isTabletInLandscape()) {
            replaceFragment(detailFragment, R.id.fragmentContainerSecondary, TAG_DETAIL_FRAGMENT)
        } else {
            addFragment(detailFragment, R.id.fragmentContainerPrimary, TAG_DETAIL_FRAGMENT)
        }
    }

    private fun replaceFragment(fragment: Fragment, containerId: Int, tag: String) {
        supportFragmentManager.beginTransaction().replace(containerId, fragment, tag).commit()
    }

    private fun addFragment(fragment: Fragment, containerId: Int, tag: String) {
        supportFragmentManager.beginTransaction().add(containerId, fragment, tag).addToBackStack(null).commit()
    }

    override fun onNavigateBack() {
        onBackPressed()
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 0) {
            supportFragmentManager.popBackStack()
        } else {
            super.onBackPressed()
        }
    }
}
