package com.xstmpx.numberslight.screen.list.view.recycler

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import com.xstmpx.numberslight.R
import com.xstmpx.numberslight.screen.list.presentation.PresentableNumbersLight

class NumbersLightAdapter(
    private var listOfNumbersLight: List<PresentableNumbersLight>,
    private val positionClickListener: PositionClickListener
) : RecyclerView.Adapter<NumbersLightViewHolder>() {

    private var selectedItem: Int? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NumbersLightViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        val view = inflater.inflate(R.layout.numberslight_item, parent, false)
        return NumbersLightViewHolder(view, positionClickListener)
    }

    override fun getItemCount(): Int {
        return listOfNumbersLight.size
    }

    override fun onBindViewHolder(viewHolder: NumbersLightViewHolder, position: Int) {
        val numbersLight = listOfNumbersLight[position]
        viewHolder.nameTextView.text = numbersLight.name
        Picasso.get()
            .load(numbersLight.image)
            .fit()
            .centerInside()
            .placeholder(R.drawable.image_placeholder)
            .error(R.drawable.image_error)
            .into(viewHolder.imageView)
        viewHolder.itemLayout.isSelected = selectedItem == position
    }

    fun setItemSelected(position: Int) {
        selectedItem?.let {
            notifyItemChanged(it)
        }
        selectedItem = position
        notifyItemChanged(position)
    }

    fun getItem(position: Int): PresentableNumbersLight {
        return listOfNumbersLight[position]
    }
}