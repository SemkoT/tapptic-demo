package com.xstmpx.numberslight.screen.common.view

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.View
import com.xstmpx.numberslight.utils.alert.ProgressAlert

open class ProgressFragment: Fragment(){
    private var progressDialog : ProgressAlert? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        progressDialog = ProgressAlert(requireContext())
    }

    fun showProgress(){
        progressDialog?.show()
    }

    fun hideProgress(){
        progressDialog?.dismiss()
    }
}