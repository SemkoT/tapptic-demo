package com.xstmpx.numberslight.screen.detail.presentation

import com.xstmpx.numberslight.rest.model.NumbersLightDetail

data class PresentableNumbersLightDetail(
    val name: String,
    val image: String,
    val text: String
) {

    companion object {
        fun from(numbersLight: NumbersLightDetail): PresentableNumbersLightDetail {
            return PresentableNumbersLightDetail(numbersLight.name, numbersLight.image, numbersLight.text)
        }
    }
}