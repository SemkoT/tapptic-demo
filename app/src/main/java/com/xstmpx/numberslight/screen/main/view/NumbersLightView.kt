package com.xstmpx.numberslight.screen.main.view

interface NumbersLightView {
    fun showDetailScreen(name: String)
    fun onNavigateBack()

}