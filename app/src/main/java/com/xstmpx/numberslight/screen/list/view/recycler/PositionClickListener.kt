package com.xstmpx.numberslight.screen.list.view.recycler

interface PositionClickListener{
    fun onPositionClicked(adapterPosition: Int)
}