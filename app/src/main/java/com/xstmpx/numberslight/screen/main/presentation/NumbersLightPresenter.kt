package com.xstmpx.numberslight.screen.main.presentation

import com.xstmpx.numberslight.screen.main.view.NumbersLightView
import javax.inject.Inject

class NumbersLightPresenter @Inject constructor() {
    private var view: NumbersLightView? = null

    fun bind(view: NumbersLightView) {
        this.view = view
    }

    fun onCreate() {

    }

    fun selectName(name: String) {
        view?.showDetailScreen(name)
    }

    fun onHomeClicked() {
        view?.onNavigateBack()
    }

}