package com.xstmpx.numberslight.screen.detail.view

interface NumbersLightDetailView{
    fun showErrorDialog()
    fun showProgress()
    fun hideProgress()
    fun showImage(image: String)
    fun showName(name: String)
    fun showText(text: String)
}