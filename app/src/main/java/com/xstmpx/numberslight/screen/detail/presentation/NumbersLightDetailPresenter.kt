package com.xstmpx.numberslight.screen.detail.presentation

import com.xstmpx.numberslight.rest.NumbersLightService
import com.xstmpx.numberslight.rest.model.NumbersLightDetail
import com.xstmpx.numberslight.screen.detail.view.NumbersLightDetailView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class NumbersLightDetailPresenter @Inject constructor(
    private val numbersLightService: NumbersLightService
) {
    private var view: NumbersLightDetailView? = null
    private var disposable: Disposable? = null
    private var name: String? = null

    fun bind(view: NumbersLightDetailView) {
        this.view = view
    }

    fun onViewCreated(name: String) {
        this.name = name
        getNumbersLightDetail()
    }

    private fun getNumbersLightDetail() {
        disposable = numbersLightService.getNumberLightDetail(name ?: return)
            .map(::toPresentableNumbersLightDetail)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribeBy(onSuccess = ::presentDetail, onError = ::onError)
        view?.showProgress()
    }

    private fun onError(throwable: Throwable) {
        view?.hideProgress()
        view?.showErrorDialog()
    }

    private fun presentDetail(numbersLightDetail: PresentableNumbersLightDetail) {
        view?.hideProgress()
        view?.showImage(numbersLightDetail.image)
        view?.showName(numbersLightDetail.name)
        view?.showText(numbersLightDetail.text)
    }

    private fun toPresentableNumbersLightDetail(numbersLightDetail: NumbersLightDetail): PresentableNumbersLightDetail =
        PresentableNumbersLightDetail.from(numbersLightDetail)

    fun unbind() {
        view?.hideProgress()
        disposable?.dispose()
        disposable = null
    }

    fun onRetryClicked() {
        getNumbersLightDetail()
    }

}