package com.xstmpx.numberslight.screen.list.view.recycler

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.xstmpx.numberslight.R

class NumbersLightViewHolder(
    itemView: View,
    positionClickListener: PositionClickListener
) : RecyclerView.ViewHolder(itemView) {
    init {
        itemView.setOnClickListener {
            positionClickListener.onPositionClicked(adapterPosition)
        }
    }
    var imageView = itemView.findViewById<ImageView>(R.id.image)
    var nameTextView = itemView.findViewById<TextView>(R.id.text)
    var itemLayout = itemView.findViewById<LinearLayout>(R.id.itemLayout)
}