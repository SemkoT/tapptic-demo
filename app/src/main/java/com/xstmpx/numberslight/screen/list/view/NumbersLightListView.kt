package com.xstmpx.numberslight.screen.list.view

import com.xstmpx.numberslight.screen.list.presentation.PresentableNumbersLight

interface NumbersLightListView {
    fun selectName(name: String)
    fun present(numbersLightList: List<PresentableNumbersLight>)
    fun showErrorDialog()
    fun showProgress()
    fun hideProgress()
}