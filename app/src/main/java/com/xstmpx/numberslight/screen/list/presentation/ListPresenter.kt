package com.xstmpx.numberslight.screen.list.presentation

import com.xstmpx.numberslight.rest.NumbersLightService
import com.xstmpx.numberslight.rest.model.NumbersLight
import com.xstmpx.numberslight.screen.list.view.NumbersLightListView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class ListPresenter @Inject constructor(
    private val numbersLightService: NumbersLightService
) {
    private var view: NumbersLightListView? = null
    private var disposable: Disposable? = null

    fun bind(view: NumbersLightListView) {
        this.view = view
    }

    fun selectName(name: String) {
        view?.selectName(name)
    }

    fun onViewCreated() {
        getNumbersLightList()
    }

    private fun getNumbersLightList() {
        view?.showProgress()
        disposable = numbersLightService.getNumbersLightList()
            .map(::toPresentableNumbersLight)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribeBy(onSuccess = ::presentList, onError = ::onError)
    }

    private fun onError(throwable: Throwable) {
        view?.hideProgress()
        view?.showErrorDialog()
    }

    private fun presentList(numbersLightList: List<PresentableNumbersLight>) {
        view?.hideProgress()
        view?.present(numbersLightList)
    }

    private fun toPresentableNumbersLight(numbersLightList: List<NumbersLight>): List<PresentableNumbersLight> {
        return numbersLightList.map { PresentableNumbersLight.from(it) }
    }

    fun unbind() {
        disposable?.dispose()
        disposable = null
    }

    fun onRetryClicked() {
        getNumbersLightList()
    }
}