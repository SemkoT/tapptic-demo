package com.xstmpx.numberslight.screen.list.view

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.xstmpx.numberslight.R
import com.xstmpx.numberslight.screen.common.view.ProgressFragment
import com.xstmpx.numberslight.screen.list.presentation.ListPresenter
import com.xstmpx.numberslight.screen.list.presentation.PresentableNumbersLight
import com.xstmpx.numberslight.screen.list.view.recycler.NumbersLightAdapter
import com.xstmpx.numberslight.screen.list.view.recycler.PositionClickListener
import com.xstmpx.numberslight.screen.main.view.NumbersLightActivity
import com.xstmpx.numberslight.utils.alert.MessageAlert
import com.xstmpx.numberslight.utils.alert.MessageAlertListener
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_list.*
import javax.inject.Inject

class NumbersLightListFragment : ProgressFragment(), NumbersLightListView, PositionClickListener, MessageAlertListener {
    companion object {
        private const val KEY_SELECTED_POSITION = "KEY_SELECTED_POSITION"
    }

    @Inject
    lateinit var presenter: ListPresenter

    private var selectedPosition: Int = 0
    private var numbersLightAdapter: NumbersLightAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidSupportInjection.inject(this)
        presenter.bind(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_list, container, false)
        restoreState(savedInstanceState)
        return view
    }

    override fun onDestroy() {
        presenter.unbind()
        super.onDestroy()
    }

    private fun restoreState(savedInstanceState: Bundle?) {
        selectedPosition = savedInstanceState?.getInt(KEY_SELECTED_POSITION) ?: 0
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt(KEY_SELECTED_POSITION, selectedPosition)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.onViewCreated()
    }

    override fun present(numbersLightList: List<PresentableNumbersLight>) {
        numbersLightAdapter = NumbersLightAdapter(numbersLightList, this)
        recyclerView.layoutManager = LinearLayoutManager(requireContext())
        recyclerView.adapter = numbersLightAdapter
    }

    override fun onPositionClicked(adapterPosition: Int) {
        numbersLightAdapter?.setItemSelected(adapterPosition)
        presenter.selectName(numbersLightAdapter?.getItem(adapterPosition)?.name ?: return)
    }

    override fun selectName(name: String) {
        val numbersLightActivity = requireActivity() as NumbersLightActivity
        numbersLightActivity.selectName(name)
    }

    override fun showErrorDialog() {
        MessageAlert.with(requireContext(), getString(R.string.error), getString(R.string.request_error), this).show()
    }

    override fun onRetryClicked() {
        presenter.onRetryClicked()
    }
}