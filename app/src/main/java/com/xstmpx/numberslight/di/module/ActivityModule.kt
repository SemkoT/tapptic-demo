package com.xstmpx.numberslight.di.module

import com.xstmpx.numberslight.screen.main.view.NumbersLightActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector


@Module
interface ActivityModule {

    @ContributesAndroidInjector()
    fun bindNumbersLightActivity(): NumbersLightActivity

}