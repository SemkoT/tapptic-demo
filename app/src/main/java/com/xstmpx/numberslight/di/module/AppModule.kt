package com.xstmpx.numberslight.di.module

import android.app.Application
import android.content.Context
import com.xstmpx.numberslight.NumbersLightsApplication
import dagger.Module
import dagger.Provides

@Module
class AppModule {

    @Provides
    fun application(app: NumbersLightsApplication): Application {
        return app
    }

    @Provides
    fun provideContext(application: Application): Context {
        return application
    }
}