package com.xstmpx.numberslight.di.module

import com.xstmpx.numberslight.screen.detail.view.NumbersLightDetailFragment
import com.xstmpx.numberslight.screen.list.view.NumbersLightListFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector


@Module
interface FragmentModule {

    @ContributesAndroidInjector()
    fun bindListFragment(): NumbersLightListFragment


    @ContributesAndroidInjector()
    fun bindDetailFragment(): NumbersLightDetailFragment

}