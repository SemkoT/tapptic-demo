package com.xstmpx.numberslight.di.component

import com.xstmpx.numberslight.NumbersLightsApplication
import com.xstmpx.numberslight.di.module.*
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton


@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        AppModule::class,
        ActivityModule::class,
        FragmentModule::class,
        NetworkModule::class
    ]
)
interface AppComponent {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: NumbersLightsApplication): Builder


        fun build(): AppComponent
    }

    fun inject(application: NumbersLightsApplication)
}
