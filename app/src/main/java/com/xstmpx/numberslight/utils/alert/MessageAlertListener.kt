package com.xstmpx.numberslight.utils.alert

interface MessageAlertListener {
    fun onRetryClicked()
}