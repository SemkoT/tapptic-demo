package com.xstmpx.numberslight.utils.alert

import android.app.AlertDialog
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import com.xstmpx.numberslight.R
import kotlinx.android.synthetic.main.message_dialog.view.*

class ProgressAlert(context: Context?) : AlertDialog(context) {

    private val dialogView: View

    init {
        val inflater = context?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        dialogView = inflater.inflate(R.layout.progress_dialog, null, false)
        this.setCancelable(false)
        this.setView(dialogView)
    }

}