package com.xstmpx.numberslight.utils.alert

import android.app.AlertDialog
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import com.xstmpx.numberslight.R
import kotlinx.android.synthetic.main.message_dialog.view.*

class MessageAlert(context: Context?, title: String, message: String, listener: MessageAlertListener?) : AlertDialog(context) {

    companion object {
        fun with(context: Context?, title: String, message: String, listener: MessageAlertListener?): MessageAlert {
            return MessageAlert(context, title, message, listener)
        }
    }

    private val dialogView: View

    init {
        val inflater = context?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        dialogView = inflater.inflate(R.layout.message_dialog, null, false)
        dialogView.messageTitle.text = title
        dialogView.messageDescription.text = message
        dialogView.positiveButton.setOnClickListener {
            dismiss()
            listener?.onRetryClicked()
        }
        this.setCancelable(false)
        this.setView(dialogView)
    }
}