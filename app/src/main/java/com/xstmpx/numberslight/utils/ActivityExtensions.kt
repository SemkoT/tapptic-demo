package com.xstmpx.numberslight.utils

import android.support.v7.app.AppCompatActivity
import com.xstmpx.numberslight.R

fun AppCompatActivity.isTabletInLandscape(): Boolean = resources.getBoolean(R.bool.isTabletInLandscape)

fun AppCompatActivity.canGoBackOnFragmentBackStack(): Boolean = supportFragmentManager.backStackEntryCount > 0